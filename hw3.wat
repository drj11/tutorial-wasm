(module
  (memory (import "js" "mem") 1)
  (func $s (import "$" "puts") (param i32))

  (func (export "hello")
    ;; Assemble the string 世界 @16 ("world" in Chinese)
    i32.const 16
    i32.const 2
    i32.store
    i32.const 20
    i32.const 0x4e16
    i32.store
    i32.const 24
    i32.const 0x754c
    i32.store
    i32.const 16
    call $s
  )
)
