(module
  (func $p (import "$" "print") (param i32))
  (func $c (import "$" "putc") (param i32))

  (func (export "hello")
    i32.const 20        ;; ASCII SP
    call $c
    i32.const 119       ;; ASCII w
    call $c
    i32.const 111       ;; ASCII o
    call $c
    i32.const 114       ;; ASCII r
    call $c
    i32.const 108       ;; ASCII l
    call $c
    i32.const 100       ;; ASCII d
    call $c
  )
)
