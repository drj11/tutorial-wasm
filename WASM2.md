# A second WASM tutorial

It will be helpful if you have done the tutorial in the
[README](README.md).

This is an excursion.
The same WASM program, but encoding it so that we can make a
standalone HTML page with all the code required, both JavaScript
and WASM, and no extra files needed.

The common approach, which the README takes, is to put the WASM
binary in another file and stream it.
This works well on the actual web, but when developing means
that you need to start a server.

When we put everything in a single standalone HTML file, we
can avoid a server. And having to understand CORS.

A WebAssembly instance can be created from an in-memory
representation of the code: a Uint8Array.

I had to hunt a little to find the right parts:

- atob
- Uint8Array.from
- charCodeAt

and it's possible that there are cleaner or simpler ways to do
this.

## Base64 and other conversions

Using good old Unix i convert the `hw.wasm` file to a
[base64 encoding](https://en.wikipedia.org/wiki/Base64)
(that's `base64 < hw.wasm`); then i can paste that as a
string in the JavaScript.

The ancient `atob` function
(seriously, it's in the original ECMAScript spec from 1997)
transforms that to... er, another string, but one which uses
char codes 0 to 255.

We can then use `Uint8Array.from` to pull this into a
_typed array_ (which we can use to make a WebAssembly module and instance).
`Uint8Array.from` copies the values from an iterator into
a fresh `Uint8Array`.

`Uint8Array.from` has a curious second argument:
a function that it maps over the iterator,
just in case you can't call _map_.
It's perfect for our use case: the values when iterating over a
string are length-1 strings; but, we can use a function to
convert those to the numbers that are their character codes.

## Sketch code

The `.wasm` file from before is only 90 bytes;
it's short enough that i can put the base64 encoded version here
(120 characters).

  // a WASM file in base64 encoding
  wasm$b64 = "AGFzbQEAAAABCAJgAX8AYAAAAhQCASQFcHJpbnQAAAEkBHB1dGMAAAMCAQEHCQEFaGVsbG8AAgohAR8AQRQQAUH3ABABQe8AEAFB8gAQAUHsABABQeQAEAEL"
  wasm$ = atob(wasm$b64)
  bs = Uint8Array.from(wasm$, (e)=>e.charCodeAt(0));

`bs` (a typed array) can then be passed to
`WebAssembly.instantiate` (instead of passing a stream to
`.instantiateStream);
it returns a promise that can be used exactly the same way as
the promise returned from `WebAssembly.instantiateSream`.

The complete working version, in a SINGLE FILE, is
[wasm2.html](wasm2.html).


## Code bloat

This method of encoding the WASM file as a Base64
JavaScript string means that the HTML is now self-contained
and no network traffic is needed.

But this comes at the cost of now having several copies of it:

- the literal string in the JavaScript text
- the runtime representation of the base64 encoded string
- the runtime representation of the base64 decoded string
- the typed array that we convert the string to

and quite possibly one or two more copies in the WebAssembly
module and its instance (the spec clearly intends for it to be
_possible_ that these are shared with the typed array used to
create the module, but who can say whether an actual
implementation does that).

In this case the entire `.wasm` file is 90 bytes. It's a trifle.

# END
