# Hello World, again

The [README](README.md) introduced my first WASM program.
It uses a JavaScript function in the host, `putc`, that
puts a single Unicode character, specified by its numeric
codepoint, onto the end of an HTML element.

That is not the only possible way to send a stream of characters
from a Web Assembly program to the JavaScript host, and
it may be that other approaches have advantages.
Sending characters one at a time from WASM to JavaScript does
seem a bit slow.

Another approach is to format the string in memory and pass a
pointer to the string back to JavaScript.

The simplest format for the string would be a length word
(32-bit), followed by a number of codepoint words (each 32-bit).
This can be placed anywhere in memory and an offset to this
location passed to the JavaScript host.


## Alignment

WASM can `i32.store` the values at any alignment
(although possibly with some speed penalty).
JavaScript, however, requires that the offset to
`new Uint32Array` is a multiple of 4.
It would be possible to decode non-aligned `uint32` values
"by hand", but it's not worth it.
WASM should 4-byte align all of its `i32` values.

# END
