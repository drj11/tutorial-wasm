# Web Assembly Tutorial

This is a sketch of a tutorial for Web Assembly (aka WASM).

You will need to be familiar with the command line and a text editor.
There are many text editors available, if you do not know one
already, i recommend starting with `nano`.

It will be useful if you know a bit of HTML and JavaScript
because i'm not explaining those parts;
although the JavaScript is small, and the HTML is tiny.

Most of what you read about Web Assembly is around the idea of
taking a program in an existing high-level language
(like C or Rust) and compiling to WASM.
This effectively treats WASM as another architecture target
for an existing compiler system.

This tutorial takes a lower-level approach:
creating a ["Hello world"
program](https://en.wikipedia.org/wiki/%22Hello,_World!%22_program) from scratch in WASM itself.


## Preliminaries

Install the [Web Assembly Binary
Toolkit](https://github.com/WebAssembly/wabt).
And Python (3, just for its web server).

    brew install wabt
    brew install python

This tutorial, including the code i ended up using, are
in a public git repo at https://gitlab.com/drj11/tutorial-wasm .
If you want it in run-ready form, clone the repo:

    git clone https://gitlab.com/drj11/tutorial-wasm.git


## WAT

The [simplest Web Assembly
Text](https://developer.mozilla.org/en-US/docs/WebAssembly/Understanding_the_text_format) is

    (module)

Put this in the file `hw.wat`;
compile it with

    wat2wasm hw.wat

A file `hw.wasm` is created.

View the assembled output with `-v` option: `wat2wasm hw.wat -v`
(this will also assemble it and overwrite the file `hw.wasm`).

Of course a program that does something must have more content;
but we compile it in the same way, with `wat2wasm`.


## JavaScript and WASM and WAT

`.wat` files don't run at all, they must be compiled (or
_assembled_ if you prefer) to `.wasm` before they can be run;
`.wasm` files don't run without some JavaScript support.

There must be some JavaScript that is responsible for
loading and running the WASM.
The JavaScript effectively provides a _runtime environment_.
In principle there could be other environments capable
of running WASM, and maybe there are, but we're talking about
JavaScript running in the browser.

The WASM code itself can perform arbitrary computations, but
has no interaction with any part of the system _except_ by
table of imports that the JavaScript runtime provides.


## Exports and Imports

Hello world as a Web Assembly program must be provided as an
exported function.
JavaScript can only enter a Web Assembly program using
the functions that the Web Assembly exports.
The exports are effectively entry points
(though you can also export things that are not functions).

Here, in `hw.wat`, there is a single export called `hello`.
The `hello` function produces output by calling the function `putc`.
`putc` is not provided by Web Assembly, it must be provided by
the JavaScript wrapper, and declared in the WASM as an import.

The binary WASM must be loaded into the JavaScript runtime
before it can be used.
There is some boilerplate code to do this.

The imports and exports are always described from the viewpoint
of the WASM.
Exports are exported _from_ Web Assembly _to_ JavaScript.
Imports are imported _to_ Web Assembly _from_ JavaScript.


## Code Actual

Time to see some code i suppose.
The whole program is quite short:

    (module
      (func $p (import "$" "print") (param i32))
      (func $c (import "$" "putc") (param i32))

      (func (export "hello")
        i32.const 20        ;; ASCII SP
        call $c
        i32.const 119       ;; ASCII w
        call $c
        i32.const 111       ;; ASCII o
        call $c
        i32.const 114       ;; ASCII r
        call $c
        i32.const 108       ;; ASCII l
        call $c
        i32.const 100       ;; ASCII d
        call $c
      )
    )

If you want to follow along at home, type this into your own
`hw.wat` file, run `wat2wasm hw.wat`, copy the [index.html
file](https://gitlab.com/drj11/tutorial-wasm/-/blob/main/index.html),
start a local web server, and pop open that page.

You can see that the text format (Web Assembly Text, WAT) is
quite Lisp-y.
The principle syntactic unit is the S-expression:
an expression inside parentheses with the operator in the first
position, just like Lisp.

The program consists of some preliminaries and some
declarations (including an unused `print` import), and
then the `hello` function itself.
The program just consists of `call` instructions;
note that the parameter comes before the call
(the WASM machine is a stack machine).


## O Hello

WASM programs can call JavaScript functions
(that have been specifically imported and made available to WASM),
but they are somewhat limited in what they can pass as parameters.
It really is like an assembly language, i can pass integers or
floats in both 32- and 64-bit formats.

My Hello World program outputs " world" (the "hello" part is
already supplied in the HTML) by sending the characters one
character-at-a-time to the `putc` function supplied by
JavaScript.

This is a bit brutal, but fairly simple.
There are other options, like using a memory object to pass
strings and other structured objects;
maybe i'll explore those in future posts.


## Serving it

CORS policies means that an actual server must be used to view
the HTML:

    python -m http.server

You'll need the WASM file `hw.wasm`, and the [index.html
file](https://gitlab.com/drj11/tutorial-wasm/-/blob/main/index.html)
which has the HTML and JavaScript needed.

With everything in place,
run `open http://[::]:8000/` which apparently is a real URL.

If everything works you should see "hello world" in the default
font of your browser.
The "hello" part is coded in the HTML already, and the " world"
part is added when the WASM code is run.
It's possible that might be useful when diagnosing problems.


## REFERENCES

[UWATF] Understanding WebAssembly text format, https://developer.mozilla.org/en-US/docs/WebAssembly/Understanding_the_text_format

# END
